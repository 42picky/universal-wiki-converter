#!/bin/bash
## Many thanks to Eric Sorenson for writing this
## BEGIN
set -ex

SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
MYCWD="$( cd -P "$( dirname "$SOURCE" )/../" && pwd )"
echo $MYCWD

CLASSPATHORIG=$CLASSPATH

CLASSPATH="$MYCWD/target/uwc/uwc.jar"
for file in lib/*.jar ; do
    CLASSPATH=$MYCWD/$file:$CLASSPATH
done

CLASSPATH=$CLASSPATH:$CLASSPATHORIG

export CLASSPATH

# run out of the sample_files dir
#cd sample_files
pushd $(pwd)
cd $MYCWD
java -Xms256m -Xmx4096m $APPLE_ARGS -classpath $CLASSPATH com.atlassian.uwc.ui.UWCCommandLineInterface $1 $2 $3 $4
popd
## END
